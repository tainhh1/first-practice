import React from 'react';

import Card from '../UI/Card';
import UserItem from './UserItem';
import style from './UserList.module.css';

const UserList = ({ userData, deleteUser }) => {
  const deleteUserHandler = (id) => {
    deleteUser(id);
  };

  return (
    <Card className={`${style.users} ${userData.length !== 0 && style.active}`}>
      <ul>
        {userData.map((user) => (
          <UserItem
            key={user.id}
            id={user.id}
            username={user.username}
            age={user.age}
            deleteUser={deleteUserHandler}
          />
        ))}
      </ul>
    </Card>
  );
};

export default UserList;
