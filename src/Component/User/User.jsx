import React, { useState, useRef } from "react";

import Card from "../UI/Card";
import Button from "../UI/Button";
import style from "./User.module.css";
import Modal from "../UI/Modal";

const User = ({ addUser }) => {
	const userNameRef = useRef();
	const ageRef = useRef();

	const [error, setError] = useState(null);

	const submitHandler = (event) => {
		event.preventDefault();
		const enteredUsername = userNameRef.current.value;
		const enteredAge = ageRef.current.value;
		if (enteredUsername.length === 0 || enteredAge.length === 0) {
			setError({
				title: `Invalid input`,
				error: `Please add a username or age for user`,
			});
			return;
		}
		if (+enteredAge < 0) {
			setError({
				title: `Invalid age input`,
				error: `Please enter a valid age > 0`,
			});
			return;
		}
		const user = {
			id: Math.random(),
			username: enteredUsername,
			age: +enteredAge,
		};
		userNameRef.current.value = "";
		ageRef.current.value = "";
		addUser(user);
	};

	const closeModalHandler = () => {
		setError(null);
	};

	return (
		<div>
			{error && (
				<Modal
					title={error.title}
					error={error.error}
					close={closeModalHandler}
				/>
			)}
			<Card className={style.input}>
				<form onSubmit={submitHandler}>
					<label htmlFor='user-name'>Username</label>
					<input ref={userNameRef} type='text' />
					<label htmlFor='age'>Age (Years)</label>
					<input ref={ageRef} type='number' />
					<Button type='submit'> Add User</Button>
				</form>
			</Card>
		</div>
	);
};

export default User;
