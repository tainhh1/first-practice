import React from 'react';

const UserItem = ({ id, username, age, deleteUser }) => {
  const onDeleteUserClick = () => {
    deleteUser(id);
  };

  return (
    <li onClick={onDeleteUserClick}>
      <p>{username}</p> <p>{`(${age} years old)`}</p>
    </li>
  );
};

export default UserItem;
