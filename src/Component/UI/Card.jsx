import React from 'react';
import styles from './Card.module.css';

const Card = ({ children, className }) => {
  const classCard = `${className} ${styles.card}`;
  return <div className={classCard}>{children}</div>;
};

export default Card;
