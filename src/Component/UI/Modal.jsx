import React from 'react';
import ReactDOM from 'react-dom';

import Button from './Button';
import style from './Modal.module.css';

const Backdrop = ({onClick}) => {
  return <div onClick={onClick} className={style.backdrop}></div>
}

const ModalOverlay = ({title, error, onClick}) => {
  return <>
    <div className={style.modal}>
        <div className={style.header}>
          <h2>{title}</h2>
        </div>
        <div className={style.content}>
          <p>{error}</p>
          <div className={style.actions}>
            <Button onClick={onClick}>Okay</Button>
          </div>
        </div>
      </div>
  </>
}

const Modal = ({ title, error, close }) => {
  return (
    <>
      {ReactDOM.createPortal(<Backdrop onClick={close} />, document.getElementById('backdrop-root'))};
      {ReactDOM.createPortal(<ModalOverlay title={title} error={error} onClick={close} />, document.getElementById('overlay-root'))}
    </>
  );
};

export default Modal;
