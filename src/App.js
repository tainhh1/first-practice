import React, { useState } from 'react';

import User from './Component/User/User';
import UserList from './Component/User/UserList';

function App() {
  const [userData, setUserData] = useState([]);

  const addUserHandler = (user) => {
    setUserData((prevState) => [...prevState, user]);
  };

  const deleteUserHandler = (id) => {
    setUserData((prevState) => prevState.filter((user) => user.id !== id));
  };

  return (
    <>
      <User addUser={addUserHandler} />
      <UserList deleteUser={deleteUserHandler} userData={userData}></UserList>
    </>
  );
}

export default App;
